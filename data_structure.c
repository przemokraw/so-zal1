/*
 * data_structure.c
 * Autor: Przemysław Krawczyk
 * Nr albumu: 305178
 */
#include "data_structure.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Odwraca kolejność elementów tablicy, zaczynając od begin, a kończąc na end */
static void reverse (char *begin, char *end) {
    char tmp;
    while (begin < end) {
        tmp = *begin;
        *begin = *end;
        *end = tmp;
        begin++; end--;
    }
}

/* Przesuwa cyklicznie elementy tablicy w lewo o k */
static void move_cyclic_to_the_left_by_k (int k, char *begin, char *end) {
    char *new_begin = begin + k - 1;
    reverse (begin, new_begin);
    reverse (new_begin+1, end);
    reverse (begin, end);
}

/* Zwraca długość symbolu */
static int get_symbol_len (char *symbol) {
    char * end = symbol;
    while (*end != ' ') end++;
    return end - symbol + 1;
}

/* Zlicza wszystkie nadmiarowe spacje w podanym wyrażeniu */
static int count_unnecessary_white_spaces (char *text) {
    int counter = 0;
    char previous = ' ';

    if (*text == '\0')
        return 0;

    while (*text != '\0') {
        if ((*text == ' ') && (previous == ' ')) counter++;
        previous = *text;
        text++;
    }
    if (*(text-1) == ' ') counter--;
    return counter;
}

size_t get_data_size (char *data) {
    int counter = 0;
    size_t size = 0;
    while (counter < 3) {
        if (*data == '\0') counter++;
        size++;
        data++;
    }
    return size;
}


void data_init (char *initial_data, int initial_data_len, char **data) {
    int i = 0;
    char previous = ' ';
    int true_len = initial_data_len - count_unnecessary_white_spaces(initial_data);
    *data = malloc(sizeof(char) * (true_len + 4)); /* + 3 na trzy znaki '\0' i +1 na dodatkową spację pomiędzy ostatnim wyrazem a pierwszym '\0' */
    while (*initial_data != '\0') {
        if ((*initial_data != ' ') || (previous != ' '))
            (*data)[i++] = *initial_data;
        previous = *initial_data;
        initial_data++;
    }
    (*data)[true_len] = ' ';
    (*data)[true_len+1] = '\0';
    (*data)[true_len+2] = '\0';
    (*data)[true_len+3] = '\0';
}

void data_clear (char **data) {
    free(*data);
}


char* expression_get_next (char *data) {
    return data;
}

char* stack_get_next (char *data) {
    while (*data != '\0') data++;
    return ++data;
}

char* get_result (char *data) {
    int counter = 0;
    while (counter < 2) {
        if (*data == '\0') counter++;
        data++;
    }
    return data;
}

enum SYMBOL symbol_type (char *symbol) {
    if (isalpha(*symbol) || isdigit(*symbol))
        return CONSTANT;
    else if (*symbol == '(')
        return LEFT_PARENTHESIS;
    else if (*symbol == ')')
        return RIGHT_PARENTHESIS;
    else
        return OPERATOR;
}

int get_operator_strength (char *symbol) {
    switch (*symbol) {
        case '^': return 2;
        case '*': ;
        case '/': return 1;
        case '+': ;
        case '-': return 0;
        default: return -1;
    }
}

int stack_is_empty (char *data) {
    return *stack_get_next(data) == '\0' ? 1 : 0;
}

int expression_is_empty (char *data) {
    return *expression_get_next(data) == '\0' ? 1 : 0;
}

void move_symbol_from_expression_to_stack (char *data) {
    char *data_begin = data;
    int symbol_len = get_symbol_len(data);
    char *stack_begin = stack_get_next(data);
    move_cyclic_to_the_left_by_k(symbol_len, data_begin, stack_begin-1);
}

void move_symbol_from_expression_to_results_queue (char *data) {
    int symbol_len = get_symbol_len(data);

    int data_len = get_data_size(data);
    char *queue_end = &data[data_len-2]; /* ostatni znak przed ostatnim '\0' */
    move_cyclic_to_the_left_by_k (symbol_len, data, queue_end);

}

void move_symbol_from_stack_to_results_queue (char *data) {
    char *stack_begin = stack_get_next(data);
    int symbol_len = get_symbol_len(stack_begin);
    char *queue_end = &data[get_data_size(data)-2];
    move_cyclic_to_the_left_by_k (symbol_len, stack_begin, queue_end);
}

void remove_symbol_from_expression (char *data) {
    int symbol_len = get_symbol_len(data);
    char *data_end = &data[get_data_size(data)-1];
    move_cyclic_to_the_left_by_k(symbol_len, data, data_end);
}

void remove_symbol_from_stack (char *data) {
    char *stack_begin = stack_get_next(data);
    int symbol_len = get_symbol_len(stack_begin);
    char *data_end = &data[get_data_size(data)-1];
    move_cyclic_to_the_left_by_k(symbol_len, stack_begin, data_end);
}
