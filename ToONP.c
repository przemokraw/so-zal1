/*
 * ToONP.c
 * Autor: Przemysław Krawczyk
 * Nr albumu: 305178
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "data_structure.h"
#include "err.h"

int send_to_child[2]; /* Pipe do przesłania danych do procesu potomnego */
int send_to_parent[2]; /* Pipe do przesłania dancych do rodzica */
pid_t pid;
size_t data_size; /* rozmiar struktury dancyh */
int to_parent; /* do przechowywania deskryptora do pisania do ojca w procesie potomnym */

void W() {
    char *loaded_data;
    char *stack_top;
    int strength; /* do przechowywania siły operatora */

    /* Zamykam odpowienie deskryptory */
    if ((close(send_to_child[1]) == -1) || (close(send_to_parent[0]) == -1))
        syserr("Error in close\n");

    loaded_data = malloc(sizeof(char) * data_size);

    /* Odczytuję dane od rodzica i zamykam niepotrzebny dalej deskryptor */
    if (read(send_to_child[0], loaded_data, data_size) == -1) {
        free(loaded_data);
        syserr("Error in read\n");
    }
    if (close(send_to_child[0]) == -1) {
        free(loaded_data);
        syserr("Error in close\n");
    }

    /* zapisuję deskryptor do pisania z ojcem do zmiennej to_parent */
    to_parent = send_to_parent[1];

    /* teraz zajmuje się przetworzeniem symbolu */
    if (expression_is_empty(loaded_data) == 1) {
        while (stack_is_empty(loaded_data) == 0)
            move_symbol_from_stack_to_results_queue(loaded_data);
       
        if (write(to_parent, loaded_data, data_size) == -1) {
            free(loaded_data);
            syserr("Error in write\n");
        }
        if (close(to_parent) == -1) syserr("Error in close\n");
        free (loaded_data);
        exit(0);
    }

    /* przypadek, kiedy został jeszcze jakiś symbol do przetworzenia */
    switch (symbol_type(loaded_data)) {
        case CONSTANT:
            move_symbol_from_expression_to_results_queue(loaded_data);
            break;
        case LEFT_PARENTHESIS:
            move_symbol_from_expression_to_stack(loaded_data);
            break;
        case RIGHT_PARENTHESIS:
            while (symbol_type(stack_get_next(loaded_data)) != LEFT_PARENTHESIS)
                move_symbol_from_stack_to_results_queue(loaded_data);
            remove_symbol_from_expression(loaded_data);
            remove_symbol_from_stack(loaded_data);
            break;
        case OPERATOR:
            strength = get_operator_strength(loaded_data);
            stack_top = stack_get_next(loaded_data);
            while ((stack_is_empty(loaded_data) == 0) && 
                   (symbol_type(stack_top) == OPERATOR) &&
                   (strength <= get_operator_strength(stack_top)))
                move_symbol_from_stack_to_results_queue(loaded_data);
            move_symbol_from_expression_to_stack(loaded_data);
            break;
    }

    /* Po przetworzeniu symbolu wysyłam resztę dancyh do potomka. Najpierw tworze pipe'y */
    if ((pipe(send_to_child) == -1) || (pipe(send_to_parent) == -1)) {
        free(loaded_data);
        syserr("Error in pipe\n");
    }

    pid = fork();
    switch (pid) {
        case -1:
            free(loaded_data);
            syserr("Error in fork\n");
        case 0:
            free(loaded_data);
            W();
            break;
        default: 
            if ((close(send_to_child[0]) == -1) || (close(send_to_parent[1]) == -1)) {
                free(loaded_data);
                syserr("Error in close\n");
            }
        
            /* Wysyłam dane do potomka */
            if (write(send_to_child[1], loaded_data, data_size) == -1) {
                free(loaded_data);
                syserr("Error in write\n");
            }
            
            /* Czekam na odbiór */
            if (read(send_to_parent[0], loaded_data, data_size) == -1) {
                free(loaded_data);
                syserr("Error in read\n");
            }
            
            if ((close(send_to_child[1]) == -1) || (close(send_to_parent[0]) == -1)) {
                free(loaded_data);
                syserr("Error in close\n");
            }

            if (wait(0) == -1) {
                free(loaded_data);
                syserr("Error in wait\n");
            }

            
            /* Wysyłam ostateczny wynik do ojca */
            if (write(to_parent, loaded_data, data_size) == -1) {
                free(loaded_data);
                syserr("Error in result\n");
            }
            if (close(to_parent) == -1) syserr("Error in close\n");

            free(loaded_data);
    }
}

void ToONP(int argc, char **argv) {
    char *data;
    char *tmp;

    if (argc != 2)
        return;

    if (!strlen(argv[1]))
        return;

    data_init(argv[1], strlen(argv[1]), &data);
    data_size = get_data_size(data);

    pipe(send_to_child);
    pipe(send_to_parent);

    pid = fork();
    switch (pid) {
        case -1:
            data_clear(&data);
            syserr("Error in fork\n");
        case 0: 
            W();
            break;
        default:
            if ((close(send_to_child[0]) == -1) || (close(send_to_parent[1]) == -1)) {
                data_clear(&data);
                syserr("Error in close\n");
            }

            if (write(send_to_child[1], data, data_size) == -1) {
                data_clear(&data);
                syserr("Error in write\n");
            }

            if (read(send_to_parent[0], data, data_size) == -1) {
                data_clear(&data);
                syserr("Error in read\n");
            }

            if (wait(0) == -1) {
                data_clear(&data);
                syserr("Error in wait\n");
            }
            tmp = &data[get_data_size(data)-1];
            if (*(tmp-1) == ' ') *(tmp-1) = '\0';
            printf("%s\n", get_result(data));
    }
    data_clear(&data);
}

int main (int argc, char **argv) {
    ToONP(argc, argv);
    return 0;
}
