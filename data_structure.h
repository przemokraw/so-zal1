/*
 * data_structure.h
 * Autor: Przemysław Krawczyk
 * Nr albumuL 305178
 */
#ifndef __DATA_STRUCTURE_H
#define __DATA_STRUCTURE_H

#include <stdio.h>

enum SYMBOL {
    CONSTANT,
    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    OPERATOR
};

/* 
 * Tworzy strukturę danych zainicjowaną wartościami z initial data.
 *
 * Ta struktura to ciągły blok pamięci zawierający 3 stringi,
 * każdy zakończony znakiem '\0', reprezentujące kolejno:
 *
 * - listę symboli wyrażenia do konwersji
 * - stos do przechowywania symboli
 * - kolejkę, w której będzie zapisywany wynik
 *
 * Kolejne symbole w stringu są od siebie oddzielone pojedynczą spacją.
 * Dla ułatwienia przeprowadzania operacji pojedyncza spacja jest również
 * na końu ostatniego symbolu w każdym stringu.
 * 
 * Tak utworzoną strukturę danych należy zlikwidować za pomocą funkcji data_clear()
 */
void data_init (char *initial_data, int initial_data_len, char **data);

/* Kasuję strukturę danych */
void data_clear (char **data);

/* Zwraca rozmiar struktury danych */
size_t get_data_size (char *data);

/* Zwraca wskaźnik na następny symbol w wyrażeniu do konwersji */
char *expression_get_next (char *data);

/* Zwraca wskaźnik na następny symbol leżący na stosie */
char *stack_get_next (char *data);

/* Zwraca wskaźnik na początek wynikowej kolejki */
char *get_result (char *data);

/* sprawdza typ symbolu.
 * zakład poprawność dancy wejściowych */
enum SYMBOL symbol_type (char *symbol);

/* Zwraca 2, jeśli symbol to '^', 1, jeśli symbol to '*', '/',
 * 0, jeśli symbol to '+' lub '-', -1, jeśli symbol nie jest opearatorem */
int get_operator_strength (char *symbol);

/* 1, jeśli stos jest pusty, 0 w p.p. */
int stack_is_empty (char *data);

/* 1, jeśli lista symboli wyrażenia do konwersji jest pusta, 0 w p.p */
int expression_is_empty (char *data);


/* UWAGA: poniższe funkcje nie sprawdzają pustości, trzeba to zrobić wcześniej */

/* Przenosi pierwszy symbol z wyrażenia do konwersji na stos */
void move_symbol_from_expression_to_stack (char *data);

/* Przenosi pierwszy symbol z wyrażenia do konwersji na koniec kolejki wynikowej */
void move_symbol_from_expression_to_results_queue (char *data);

/* Przenosi element z wierzchu stosu na koniec kolejki wynikowej */
void move_symbol_from_stack_to_results_queue (char *data);

/* Usuwa następny symbol z wyrażenia do konwersji */
void remove_symbol_from_expression (char *data);

/* Usuwa symbol z wierzchu stosu */
void remove_symbol_from_stack (char *data);

#endif /* __DATA_STRUCTURE_H */
