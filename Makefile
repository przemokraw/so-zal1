CC=gcc
CFLAGS=-Wall --pedantic -c -fsanitize=address
LDFLAGS=-Wall --pedantic -fsanitize=address

EXE=ToONP

all: $(EXE)

$(EXE): ToONP.c data_structure.o err.o
	$(CC) $(LDFLAGS) -o ToONP ToONP.c data_structure.o err.o

data_structure.o: data_structure.c
	$(CC) $(CFLAGS) -o data_structure.o data_structure.c

err.o: err.c
	$(CC) $(CFLAGS) -o err.o err.c

clean:
	rm -f *.o ToONP *~
